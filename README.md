# SweetBat
Battery sweet spot notifier. It notifies you when your battery is over-charged, over-discharged, and in the sweet spot.

The notifier should be compatible with both KDE and Gnome, however it has only been tested on KDE.

## Get started
### Tunables
The following variables can be modified as needed by opening the sweetbat script in an editor. Note that you will need to reinstall after modification although you could edit the installed copy directly too.
| Vars.              | Description                                                             |
|--------------------|-------------------------------------------------------------------------|
| `high_threshold`   | Threshold after which the battery is considered to be over-charging.    |
| `low_threshold`    | Threshold after which the battery is considered to be over-discharging. |
| `NOTIFY_ON_TURNON` | Show a notification when the service starts, i.e. on system start up.   |
### Install
To install the notifier clone the repo and run the install script.
```
git clone https://github.com/JKaffe/SweetBat.git
cd SweetBat
sudo ./install -i
```
You should see a notification pop up right away.

### Uninstall
To uninstall:
```
sudo ./install -u
```

## Troubleshooting
### BAT1 not found.
It's likely that your system might be using something other than BAT1 for your battery. Look through the power supply folder and identify the battery.
```
ls /sys/class/power_supply
```
Once identified open the sweetbat script and change the BAT_CAP_FILE variable to match your system.
You will need to reinstall the notifier.
